# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2013-2015 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by sebastien.pulver on 07.12.2016.
#
# imports of python lib
import logging

# imports of openerp
from openerp import models, fields, api, _

# imports from odoo modules

# global variable definitions
_logger = logging.getLogger(__name__)


class ProductTemplate(models.Model):
    # Private attributes
    _inherit = 'product.template'

    # Default methods

    # Fields declaration

    # compute and search fields, in the same order that fields declaration
    @api.multi
    def name_get(self):
        """Format displayed name"""
        result = super(ProductTemplate, self).name_get()
        if not self.env.context.get('show_module_name'):
            return result

        new_result = []
        for record in self:
            entry = [r for r in result if r[0] == record.id][0]
            new_result.append(
                    (record.id, u'{1} ({0})'.format(
                            entry[1],
                            record.module_name)))
        return new_result

        # Constraints and onchanges

        # CRUD methods

        # Action methods

        # Business methods
