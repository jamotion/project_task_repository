# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2013-2015 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by renzo.meister on 13.06.2015.
#
from openerp import models, fields
import logging

_logger = logging.getLogger(__name__)


class ProjectTaskRepository(models.Model):
    # Private attributes
    _inherit = 'project.task'

    modules_edit_ids = fields.Many2many('product.template',
                                        'product_template_1_rel',
                                        'module_tag',
                                        'product_id',
                                        string='Modules to edit / create',
                                        domain=[('is_module', '=', True)])

    modules_depending_ids = fields.Many2many('product.template',
                                             'product_template_2_rel',
                                             'module_tag',
                                             'product_id',
                                             string='Depending modules',
                                             domain=[('is_module', '=', True)])
