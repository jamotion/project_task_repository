Changelog
=========
Version 8.0.1.1.1
-----------------
- Hotfix
    Ascii-error when tagging modules to create / edit

Version 8.0.1.1.0
-----------------
- Task 1475:
    Enhanced module tags (Module name and technical name) for modules of a task

Version 8.0.1.0.0
-----------------
- Feature 1312: Initial release
    Added new fields "Modules To Edit / Create" and "Depending Modules" for Module
    Tags on Task form view