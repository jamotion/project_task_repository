# -*- coding: utf-8 -*-
{
    'name': 'Project Task Repository',
    'version': '8.0.1.1.1',
    'category': 'Tools',
    'author':  'Jamotion GmbH',
    'website': 'https://jamotion.ch',
    'summary': 'Show modules to edit / create and depending modules on task',
    'images': [],
    'depends': [
        'project', 'product_module_info',
    ],
    'data': [
         'views/project_task_view.xml',
    ],
    'demo': [],
    'test': [],
    'application': False,
    'active': False,
    'installable': True,
}
